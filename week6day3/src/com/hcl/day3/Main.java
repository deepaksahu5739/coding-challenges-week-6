package com.hcl.day3;


public class Main {
	
	  public static void main(String[] args) {
	      ShapeDraw shapeDraw = new ShapeDraw();

	    
	      TwoDShape shape1 = shapeDraw.getShape("CIRCLE");

	     
	      shape1.draw();

	      
	      TwoDShape shape2 = shapeDraw.getShape("RECTANGLE");

	    
	      shape2.draw();

	     
	      TwoDShape shape3 = shapeDraw.getShape("TRIANGLE");

	      
	      shape3.draw();
     
     

	}


}